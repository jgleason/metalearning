# Metalearning

To facilitate better metalearning we can collaboratively build a dataset
useful for metalearning. Such dataset would consist of samples of the following:
 * problem
 * dataset
 * pipeline
 * runtime arguments used
 * outcome
 * other metadata (context)

The aim of this repository is to standardize how this data is represented
so that it can be useful to all performers. Moreover, for easier sharing and
consumption of this data, some common utilities and services might be developed.

## Considerations

* Pipelines should describe also interactions by the user.
  * We should store some user identifier so one can learn across users.
* All pipelines we are collecting should be ran pipelines at least once.
  * Ideally, outcome object would be (approximately )the same if pipeline is rerun with same resources available.
* Outcome object should contain:
  * metrics on input data
  * resources used for running
    * time, memory, compute used
    * per primitive and for the whole pipeline
  * user identifier
  * did user choose the pipeline
  * a textual description why user chose the pipeline
  * failure - why it failed
    * for example, out of memory issue
  * when was it discovered:
    * during pretraining
    * during evaluation
* Same pipeline descriptions can be used also in TA2-TA3 API:
  * To describe the pipeline to the user.
  * To tell TA2 system which pipeline to build using placeholders (and TA2 system then autocompletes the pipeline).
* We should also include attribution for pipelines:
  * Which team did it, source.
  * Timestamp.
* How we standardize same cross validation and general loop of preprocessing (dataset splitting) and score computation,
  so that outcome object is useful and share same properties? If every pipeline can be anything, then outcomes will
  be hard to compare for the same dataset. But if we know that dataset splitting and score computation is done the same,
  then we can compare outcome objects.

# Database structure

Because there are one-to-many and many-to-many relations between pipeline run and problem, dataset, and pipeline,
those three are stored each in its own database collection and referenced from a pipeline run document.
So in JSON, one pipeline run could be represented as:

```yaml
{
  "id": <pipeline run ID, uuid based on content without id field>,
  "schema": <overall pipeline run schema version/URI>,
  "problem": {
    "id": <problem document ID, chosen by creator>,
    "digest": <sha256 hash of problem document>
  },
  "datasets": [
    {
      "id": <dataset document ID, chosen by creator>,
      "digest": <sha256 hash of dataset document>
    }
  ],
  "pipeline": {
    "id": <pipeline document ID, chosen by creator>,
    "digest": <sha256 hash of pipeline document>
  },
  <pipeline run fields>
}
```

Problem, dataset, and pipeline documents are referenced by both `id` and
`digest` at the top-level of the pipeline run document. They are referenced
just by `id` at deeper levels.

For future compatibility, each run can reference multiple datasets.

Pipeline is a reference as well because for each pipeline multiple runs on different set of resources and hyper-parameter
configurations can be made.

In storage, we can denormalize database structure and inline referenced objects to make queries
easier, if we will use a database which does not support joins.

The id for pipeline run document is generated in a consistent, reproducible way,
using UUID:

```python
import json
import uuid

HASH_ID_NAMESPACE = uuid.UUID('8614b2cc-89ef-498e-9254-833233b3959b')
pipeline_run.id = uuid.uuid5(
  HASH_ID_NAMESPACE,
  json.dumps(pipeline_run.to_json_structure(without_id=True), sort_keys=True)
)
```

The `id` is generated on pipeline run documents without the ID field and then
validated with the `id` field. The `id` can also be used to de-duplicate
documents.


## Problem

We use [problem description](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/problem.json)
to describe problems. Because problem IDs are not necessary unique (there can
be multiple versions with same problem ID), we use `digest` to identify a
particular problem. Example:

```yaml
{
  "id": "iris_problem_1",
  "digest": <sha256 hash of document>,
  "version": "1.0",
  "description": "Distinguish Iris flowers of three related species.",
  "name": "Distinguish Iris flowers",
  "problem": {
    "performance_metrics": [
      {
        "metric": "ACCURACY"
      }
    ],
    "task_subtype": "MULTICLASS",
    "task_type": "CLASSIFICATION"
  },
  "inputs": [
    {
      "dataset_id": "iris_dataset_1",
      "targets": [
        {
          "target_index": 0,
          "resource_id": "0",
          "column_index": 5,
          "column_name": "species"
        }
      ]
    }
  ],
  "outputs": {
    "predictions_file": "predictions.csv",
    "scores_file": "scores.csv"
  }
}
```

It might happen that there exist multiple equal or equivalent problem documents with different problem document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).

## Dataset

We use [dataset schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/container.json)
to describe datasets. Similar to problems, also dataset IDs are not unique, so
the pairing of `id` and `digest` identifies a particular dataset. Dataset
document stored in the database could consist of all top-level metadata (and
metafeatures) of a dataset, including metadata necessary to retrieve a copy of
a dataset. But when using common D3M datasets only two fields necessary to
identify a dataset are required. Example for full top-level metadata:

```yaml
{
  "id": "iris_dataset_1",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
  "version": "1.0",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55",
  "structural_type": "d3m.container.Dataset",
  "name": "Iris Dataset",
  "location_uris": [
    "https://gitlab.com/datadrivendiscovery/tests-data/raw/master/datasets/iris_dataset_1/datasetDoc.json"
  ],
  "dimension": {
    "name": "resources",
    "semantic_types": [
      "https://metadata.datadrivendiscovery.org/types/DatasetResource"
    ],
    "length": 1
  },
  "source": {
    "license": "CC",
    "redacted": false,
    "human_subjects_research": false
  }
}
```

And only required fields:

```yaml
{
  "id": "iris_dataset_1",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55"
}
```

It might happen that there exist multiple equal or equivalent dataset documents with different dataset document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).
In the case of datasets, one can use dataset's digest to determine which datasets share the same dataset description
and files.

Dataset digest is computed over the stored dataset in the D3M dataset format and includes both metadata and data (files).

## Pipeline

We use [pipeline schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/pipeline.json)
to describe a pipeline. Pipeline `id`s are not unique, so `digest` is also used
to uniquely identify a pipeline.

Moreover, equal or equivalent pipelines can happen to exist in the database even with different pipeline IDs.
It is not required from systems to assure that there are no duplicates of this kind either. Generally
the idea is simple: if a pipeline was generated by your system, create a new entry into the database for it,
listing yourself as an author, and store results of running it. If you find a pipeline by searching over this
database, then do not create a new entry, but just record results you got running it.

For a pipeline run, all primitives and subpipelines have to be exactly resolved (including the digest).
When the same system both produced a pipeline and run it, this is easy to assure. But if pipelines
are run later on, exact same primitives and subpipelines might not be found. In that case the system
should resubmit a new pipeline with updated primitives and subpipelines and then submit a pipeline
run for that updated pipeline.

## Pipeline run fields

### Steps and their execution

During run of a pipeline, primitives receive a `hyperparams` constructor argument, and
their methods can receive also extra arguments which do not depend on pipeline data. They
might be runtime arguments or overriding hyper-parameters for a given call (arguments of
kind `RUNTIME` and `HYPERPARAMETER` in primitive's metadata, respectively). `steps` field
provides values for all of those. Moreover, each method can be called multiple times, in
an iterative fashion. This is also recorded in this field. Steps also record information
about their execution. In JSON:

```yaml
# In same order as they are listed in the pipeline. Each step
# corresponds to a step in the pipeline, order-wise.
"steps": [
  {
    # Does this step corresponds to a primitive or a subpipeline?
    "type": <"PRIMITIVE", "SUBPIPELINE">,
    # Together with hyper-parameters listed as part of a pipeline they complete all values necessary to
    # instantiate "hyperparams" constructor argument. All hyper-parameter values have to be listed explicitly,
    # even if the value matches the default value of a hyper-parameter.
    "hyperparams": {
      <map between hyper-parameter names and their values>
    },
    "random_seed": <integer>,
    # Information about method calls, in order in which they were called.
    "method_calls": [
      {
        "name": "__init__",
        # Arguments to constructor should not be provided, because they are provided by runtime
        # and are runtime specific (paths to volumes, etc.).
        ...
      },
      {
        "name": "set_training_data"
        # Pipeline arguments to methods are provided in a standard way. But methods can
        # have additional runtime arguments or arguments overriding hyper-parameters for a call.
        # Those values have to be explicitly listed here.
        "arguments": {}
        # Python LogRecord entries during a method run.
        # Other custom fields are allowed (Python LogRecord can be extended with custom fields).
        "logging": [
          # See: https://docs.python.org/3/library/logging.html#logging.LogRecord
          {
            "name": ...,
            "msg": ..., # Non-interpolated message.
            "args": [ ], # Arguments for message interpolation, when JSON-serializable.
            "levelname": ...,
            "levelno": ...,
            "pathname": ...,
            "filename": ...,
            "module": ...,
            "exc_text": ..., # Python exception and formatted stack trace as text.
            "exc_type": ..., # Python exception name.
            "stack_info": ..., # Formatted stack trace as text.
            "lineno": ...,
            "funcName": ...,
            "created": ...,
            "msecs": ...,
            "relativeCreated": ...,
            "thread": ...,
            "threadName": ...,
            "processName": ...,
            "process": ...,
            "message": ..., # Interpolated message.
            "asctime": ...
          },
          ...
        ],
        # Metadata of the "CallResult" value (or "MultiCallResult" values) if the method
        # call returns a container type.
        "metadata": {
          # For "CallResult", we store metadata under "value" key.
          "value": [
            {
              # Serialized metadata as selector/metadata pairs.
              "selector": <selector>,
              "metadata": <metadata>
            }
          ],
          # For "MultiCallResult", keys should match "values" names.
          ...
        }
        # Status for the method call.
        "status": {
          "state": <"SUCCESS" or "FAILURE">,
          "message": <failure message>
        },
        "start": <method run start, absolute timestamp>,
        "end": <method run end, absolute timestamp>
      },
      {
        "name": "fit",
        "arguments": {
          # Let's assume this primitive has a hyper-parameter called "threshold" and an argument on
          # "fit" allowing one to override it. Then here we record which value was used in this call
          # for this argument.
          "threshold": <value>,
          # The following are standard runtime arguments, we list their defaults values we used.
          "timeout": null,
          "iterations": null
        }
      },
      {
        "name": "fit",
        # Specifying "calls" allows one to combine multiple calls with same arguments into one record.
        # In this example, this means that this method has been called twice more.
        "calls": 2,
        "arguments": {
          "threshold": <value>,
          "timeout": null,
          "iterations": null
        }
        ...
      },
      {
        "name": "produce",
        "arguments": {
          "timeout": null,
          "iterations": null
        },
        # optional environment if this method_call was run in a different
        # than the global or step level environment
        "environment": <same as top-level environment structure>
        ...
      }
    ],
    # If this step is a sub-pipeline, then here come steps of a sub-pipeline, recursively.
    # In this case field above are not populated.
    "steps": [
      ...
    ],
    # The following fields are used both for "PRIMITIVE" and "SUBPIPELINE".
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    },
    # optional environment if this step was run in a different environment than
    # the global environment specified at the top level
    "environment": <same as top-level environment structure>
  }
],
# Status for the whole pipeline run.
"status": {
  "state": <"SUCCESS" or "FAILURE">,
  "message": <failure message>
}
```

### Run fields

Run fields describe how a pipeline was run and results.
If pipeline is run multiple times during evaluation, each run should have its own pipeline run entry.
In JSON, the following is a sketch of its representation:

```yaml
"run": {
  # Custom phases are allowed (like one for end-to-end refinement).
  # In the case of custom phases mostly only all method calls should be recorded and results only if it produces any.
  "phase": <a string like "FIT", "PRODUCE", or some other string representing a phase this pipeline run is associated with>,
  "fold_group": {
    "id": <UUID of the fold group of pipeline runs belonging together, e.g., part of the same cross-validation evaluation run>,
    # A fold number for this pipeline run. If not part of the cross-validation, this can be set to 0.
    "fold": <integer>
  },
  # If a data preparation pipeline was used in this pipeline run, we reference the pipeline here.
  "data_preparation": {
    "pipeline": {
      "id": <ID of the pipeline which prepares data>,
      "digest": <sha256 hash of pipeline document>
    },
    "steps": [
      # Same steps record as for the regular pipeline (random_seed, hyperparams, status, etc.).
      ...
    ],
    # status for the data_preparation pipeline.
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    }
  },
  # This pipeline can be used for any pipeline run type, because it computes scores.
  "scoring": {
    "pipeline": {
      "id": <ID of the pipeline which processes outputs from the pipeline to compute the score>,
      "digest": <sha256 hash of pipeline document>
    },
    "steps": [
      # Same steps record as for the regular pipeline (random_seed, hyperparams, status, etc.).
      ...
    ],
    # status for the scoring pipeline.
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    }
  },
  # If a phase produces any results and/or scores, we record them.
  "results": {
    # Scores should the match the output of the scoring pipeline.
    # Any custom metric name should match the metric name in the scoring pipeline output.
    "scores": [
      {
        "metric": {
          "metric":  <metric name>,
          "params": {
            "k": ...,
            "pos_label": ...,
          }
        },
        "dataset_id": <ID of the dataset which contains the targets for this score>,
        "targets": [
          {
            "target_index": 0,
            "resource_id": "0",
            "column_index": 5,
            "column_name": "species"
          }
        ],
        "value": <value>
      }
    ],
    # Produced data for this run.
    "predictions": {
      "header": [
        <a list of column names>
      ],
      "values": [
        <a list of predictions themselves, every element is a list of column values, in Lincoln Labs predictions format>
      ]
    }
  }
}
```

Preparation of the data and any postprocessing should be described using standard pipelines for
known evaluation approaches. Preparation pipeline maps input datasets to output datasets in a
way that the whole pipeline is optional if data was not changed before being passed to the main
pipeline. Similarly, scoring pipeline is used only to compute scores from predictions.

### Context fields

There are additional fields to store other metadata for a pipeline run. In JSON:

```yaml
"context": <"PRETRAINING", "TESTING", "EVALUATION", "PRODUCTION">,
# References a pipeline_run that occurred immediately before this pipeline_run.
# Used for reproducibility, for example a test run would reference the train
# run. If it is not provided, it indicates the first pipeline run.
"previous_pipeline_run": {
  "id": <pipeline_run document ID>
}
"users": [
  {
    "id": <Globally unique ID for this user. It can be opaque, but it should identify the same user across sessions. Consider using UUID variant 5 with namespace set to the name of your system and name to an ID in your system's database.>,
    "reason": <A natural language description of what the user did to be on the list, e.g., \"Picked a pipeline from a list of pipelines.\".>
    "rationale": <A natural language description by the user of what the user did, e.g., \"I picked a pipeline because it looks short in comparison with others.\".>
  }
],
# A description of the environment in which this pipeline_run occurred. It is
# assumed that all `steps` and `method_calls` were run in this environment,
# unless an environment is specified at the step or method_call level.
"environment": {
  "id": <UUID based on `environment`>

  # ID is decided by whoever runs it, but it should be globally unique.
  # The idea is that the worker specifies the system inside which the pipeline
  # is run so that multiple runs on the same system can be grouped together.
  "worker_id": <globally unique ID for the worker>,

  # A git version of your (or reference, if directly using it) engine. This can
  # be useful for the author of the pipeline run to record, but is less useful
  # for others. For others, "reference_engine_version" is probably more useful.
  "engine_version": <git commit hash of the engine version used>,

  # If your engine is subclassing a reference engine, list its git version here.
  "reference_engine_version": <git commit hash of the reference engine version based on>,

  # If a pipeline is run inside a Docker container, this field should specify
  # the Docker image used to run this pipeline. This can be useful for
  # the author of the pipeline run to record, but is less useful for others.
  # For others, "base_docker_image" is probably more useful.
  "docker_image": {
    # Docker image name including a label, and optionally prefixed with a registry.
    "image_name": <value>,
    "image_digest": <SHA256 Docker image digest>
  },

  # If a pipeline is run inside a Docker container which is based on a public
  # image or known base image, then this field should specify that Docker image.
  # I.e., if your system is using a private Docker image but is extending a
  # "complete" Docker image, then list the "complete" Docker image here.
  "base_docker_image": {
    # Docker image name including a label, and optionally prefixed with a registry.
    "image_name": <value>,
    "image_digest": <SHA256 Docker image digest>
  },

  # Resources available for this environment.
  "resources": {
    "cpu": {
      "devices": [
        {
          "name": <phycical CPU device name>
        }
      ],
      "physical_present": <how many physical CPU cores are available>,
      "logical_present": <how many logical CPU cores are available>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-cpu
      # for more information.
      "configured_available": <the amount of CPU resources available to the pipeline runtime in Kubernetes CPU units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    "memory": {
      "devices": [
        {
          "name": <phycical memory device name>,
          "memory": <memory in bytes for this device>
        }
      ],
      "total_memory": <how much memory is available in bytes>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory
      # for more information.
      "configured_memory": <the amount of memory available to the pipeline runtime in Kubernetes memory units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    "gpu": {
      "devices": [
        {
          "name": <physical GPU device name>,
          "memory": <memory in bytes for this GPU>
        }
      ],
      "total_memory": <how much GPU memory is available in bytes>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory
      # for more information.
      "configured_memory": <the amount of GPU memory available to the pipeline runtime in Kubernetes memory units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    # Reference benchmarks are pipeline runs of standard and optional additional benchmark
    # pipelines which should be run on the worker during same or equivalent session so that
    # this pipeline run can be expected to have the same timing characteristics. If it is
    # known that worker configuration has not been changed between sessions, benchmark
    # pipeline runs can be reused.
    "reference_benchmarks": [
      {
        "id": <pipeline run IDs>
      }
    ]
  }
}
```

### Pipeline description example
```python
from d3m.metadata.pipeline import Pipeline, PrimitiveStep, ArgumentType, PipelineContext
from d3m import primitives

# Denormalize -> DatasetToDataFrame -> MetafeatureExtractor

# Creating Pipeline
pipeline_description = Pipeline(context=PipelineContext.TESTING)
pipeline_description.add_input(name='inputs')

# Step 0: Denormalize
step_0 = PrimitiveStep(primitive_description=primitives.datasets.Denormalize.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: DatasetToDataFrame
step_1 = PrimitiveStep(primitive_description=primitives.datasets.DatasetToDataFrame.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 2: ColumnParser
step_2 = PrimitiveStep(primitive_description=primitives.data.ColumnParser.metadata.query())
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 3: MetafeatureExtractor
step_3 = PrimitiveStep(primitive_description=
                            primitives.byudml.metafeature_extraction.MetafeatureExtractor.metadata.query())
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_output('produce')
pipeline_description.add_step(step_3)

# Adding output step to the pipeline
pipeline_description.add_output(name='Metafeatures', data_reference='steps.3.produce')

with open('pipeline.json', 'w') as write_file:
    write_file.write(pipeline_description.to_json(indent=4, sort_keys=True, ensure_ascii=False))
```
#### Output
```json
{
  "id": "75034a2f-7723-4257-b1f7-f64013905155",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
  "created": "2018-06-04T21:05:32.233121Z",
  "context": "PRODUCTION",
  "inputs": [
    {
      "name": "inputs"
    }
  ],
  "outputs": [
    {
      "data": "steps.3.produce",
      "name": "Metafeatures"
    }
  ],
  "steps": [
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "f31f8c1f-d1c5-43e5-a4b2-2ae4a761ef2e",
        "version": "0.2.0",
        "python_path": "d3m.primitives.datasets.Denormalize",
        "name": "Denormalize datasets",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.0.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "4b42ce1e-9b98-4a25-b68e-fad13311eb65",
        "version": "0.2.0",
        "python_path": "d3m.primitives.datasets.DatasetToDataFrame",
        "name": "Dataset to DataFrame converter",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.1.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "d510cb7a-1782-4f51-b44c-58f0236e47c7",
        "version": "0.2.0",
        "python_path": "d3m.primitives.data.ColumnParser",
        "name": "Parses strings into their types",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.2.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "28d12214-8cb0-4ac0-8946-d31fcbcd4142",
        "version": "0.4.0",
        "python_path": "d3m.primitives.byudml.metafeature_extraction.MetafeatureExtractor",
        "name": "Dataset Metafeature Extraction",
        "digest": "5d317e40543cc5d9e8b4431514197affc07ecd7d43bc23decb21a17e41f45fd9"
      }
    }
  ]
}
```

